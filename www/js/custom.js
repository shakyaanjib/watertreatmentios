// modal to get and set the xml range and parameter values
var xmlvalues = (function() {
    var conductivity = 0, tds = 0, ph_max = 0, ph_min = 0, ebct = 0, dm = 0, fv = 0, 
        mp = 0, selenium_max = 0, selenium_min = 0, arsenic_max = 0, arsenic_min = 0, 
        cr_max = 0, cr_min = 0, floride_max = 0, floride_min = 0, tt, contaminant,
		hlrRangeMin = 0, hlrRangeMax = 0, heightMax = 0, heightMin = 0, pi = 0;
    
    function setSeleniumRange(upper, lower) {
        selenium_max = upper;
        selenium_min = lower;
    } 
    
    function getSeleniumRange() {
        var range = {upper: selenium_max, lower: selenium_min};
        return range;
    }
    
    function setArsenicRange(upper, lower) {
        arsenic_max = upper;
        arsenic_min = lower;
    } 
    
    function getArsenicRange() {
        var range = {upper: arsenic_max, lower: arsenic_min};
        return range;
    }
    
    function setCrRange(upper, lower) {
        cr_max = upper;
        cr_min = lower;
    } 
    
    function getCrRange() {
        var range = {upper: cr_max, lower: cr_min};
        return range;
    }
    
    function setFlorideRange(upper, lower) {
        floride_max = upper;
        floride_min = lower;
    } 
    
    function getFlorideRange() {
        var range = {upper: floride_max, lower: floride_min};
        return range;
    }
    
    function setConductivity(c) {
        conductivity = c;
    } 
    
    function getConductivity() {
        return conductivity;
    }
    
    function setTDS(t) {
        tds = t;
    } 
    
    function getTDS() {
        return tds;
    }
    
    function setPHRange(upper, lower) {
        ph_max = upper;
        ph_min = lower;
    } 
    
    function getPHRange() {
        var range = {upper: ph_max, lower: ph_min};
        return range;
    }
    
    function setEBCT(e) {
        ebct = e;
    }
    
    function getEBCT() {
        return ebct;
    }
    
    function setParameters(d, v, p, hlrMin, hlrMax, hmin, hmax, piValue) {
        dm = d;
        fv = v;
        mp = p;
		hlrRangeMin = hlrMin;
		hlrRangeMax = hlrMax;
		heightMax = hmax;
		heightMin = hmin;
		pi = piValue;
    }
    
    function getParameters() {
        var params = {dm: dm, fv: fv, mp: mp, hlrRangeMin: hlrRangeMin, hlrRangeMax: hlrRangeMax, heightMax: heightMax, heightMin: heightMin, pi: pi};
        return params;
    }
    
    function setTreatmentType(type) {
        tt = type;
    }
    
    function getTreatmentType() {
        return tt;
    }
    
    return {
        setSeleniumRange: setSeleniumRange,
        getSeleniumRange: getSeleniumRange,
        setArsenicRange: setArsenicRange,
        getArsenicRange: getArsenicRange,
        setCrRange: setCrRange,
        getCrRange: getCrRange,
        setFlorideRange: setFlorideRange,
        getFlorideRange: getFlorideRange,
        setConductivity: setConductivity,
        getConductivity: getConductivity,
        setTDS: setTDS,
        getTDS: getTDS,
        setEBCT: setEBCT,
        getEBCT: getEBCT,
        setPHRange: setPHRange,
        getPHRange: getPHRange,
        setParameters: setParameters,
        getParameters: getParameters,
        setTreatmentType: setTreatmentType,
        getTreatmentType: getTreatmentType
    };
})();

function validateForm() {
    var $selenium = $("#selenium_tb"),
            $arsenic = $("#arsenic_tb"),
            $cr = $("#cr_tb"),
            $floride = $("#floride_tb");
    
    var s_range = xmlvalues.getSeleniumRange();
    var a_range = xmlvalues.getArsenicRange();
    var cr_range = xmlvalues.getCrRange();
    var f_range = xmlvalues.getFlorideRange();

    var s_lower = parseFloat(s_range.lower),
            s_upper = parseFloat(s_range.upper),
            a_lower = parseFloat(a_range.lower),
            a_upper = parseFloat(a_range.upper),
            c_lower = parseFloat(cr_range.lower),
            c_upper = parseFloat(cr_range.upper),
            f_lower = parseFloat(f_range.lower),
            f_upper = parseFloat(f_range.upper);

    var s_val = parseFloat($selenium.val().trim()),
            a_val = parseFloat($arsenic.val().trim()),
            c_val = parseFloat($cr.val().trim()),
            f_val = parseFloat($floride.val().trim());
    
    var $ph = $("#ph_tb");
    var ph_val = parseFloat($ph.val().trim());
        
    var pn = $("#projectname_tb").val().trim(),
        pl = $("#projectleader_tb").val().trim(),
        wwt = $("#wastewater_tb").val().trim(),
        cn = $("#client_tb").val().trim();    
    
    var currTDS = $("#tds_tb").val().trim();
    var currConductivity = $("#conductivity_tb").val().trim();
    
    var hasinvalid = $("#form input").hasClass("is-invalid");
    if (hasinvalid) {
        $("#form input").removeClass("is-invalid");
    }
    $("#form .invalid-tooltip").hide();
    
    if(pn === "") {
        $("#projectname_tb").addClass("is-invalid");
        $("#pn-invalid").show();
        return false;
    }
    
    if(pl === "") {
        $("#projectleader_tb").addClass("is-invalid");
        $("#pl-invalid").show();
        return false;
    }
	 if(cn === "") {
        $("#client_tb").addClass("is-invalid");
        $("#cn-invalid").show();
        return false;
    }
    
    if(wwt === "") {
        $("#wastewater_tb").addClass("is-invalid");
        $("#wwt-invalid").show();
        return false;
    }
    
   
    
    if (!$("#treatmentType1")[0].checked && !$("#treatmentType2")[0].checked) {
        $("#type-invalid").css('top', '80%');
        $("#type-invalid").show();
        return false;
    }
    
    if (isNaN(s_val) && isNaN(a_val) && isNaN(c_val) && isNaN(f_val)) {
        $("#selenium_tb, #arsenic_tb, #cr_tb, #floride_tb").addClass("is-invalid");
        $("#contaminant-invalid").show();
        return false;
    }
	if (s_val == 0 && a_val == 0 && c_val == 0 && f_val == 0) {
        $("#selenium_tb, #arsenic_tb, #cr_tb, #floride_tb").addClass("is-invalid");
        $("#contaminant-invalid").show();
        return false;
    }
    if (s_val != 0)
	{
		if (!isNaN(s_val) && (s_val > s_upper)) {
			$("#selenium_tb").addClass("is-invalid");
			$("#sel-invalid").show();
			return false;
		}
	
	}
	if (a_val != 0)
	{
			if (!isNaN(a_val) && ( a_val > a_upper)) {
				$("#arsenic_tb").addClass("is-invalid");
				$("#a-invalid").show();
				return false;
			}
	}
    
    if (c_val != 0)
	{
		if (!isNaN(c_val) && ( c_val > c_upper)) {
			$("#cr_tb").addClass("is-invalid");
			$("#cr-invalid").show();
			return false;
		}
	}

	if(f_val != 0)
	{
		if (!isNaN(f_val) && ( f_val > f_upper)) {
			$("#floride_tb").addClass("is-invalid");
			$("#f-invalid").show();
			return false;
		}
	}
	
	if ($("#q_tb").val().trim() === "") {
        $("#q_tb").addClass("is-invalid");
        $("#q-invalid").show();
        return false;
    }
	
	if ($("#q_tb").val().trim() != "") {
		if ($("#q_tb").val() > 100 ) {
			$("#q_tb").addClass("is-invalid");
			$("#qOut-invalid").show();
			return false;
		}
        
    }

    if (isNaN(ph_val)) {
        $("#ph_tb").addClass("is-invalid");
        $("#ph-invalid").text("Please enter the pH");
        $("#ph-invalid").show();
        return false;
    }
    
   
    
    if($("#treatmentType1")[0].checked && currTDS === "") {
        $("#tds_tb").addClass("is-invalid");
        $("#tds-invalid").text("Please enter the TDS value");
        $("#tds-invalid").show();
        return false;
    }
    
    if($("#treatmentType2")[0].checked && currConductivity === "") {
        $("#conductivity_tb").addClass("is-invalid");
        $("#c-invalid").text("Please enter the Conductivity value");
        $("#c-invalid").show();
        return false;
    }
    
    return true;
}

$(window).on('load', function () {
    var s_upper = 0, s_lower = 1, a_upper = 0, a_lower = 1,
            cr_upper = 0, cr_lower = 1, fl_upper = 0, fl_lower = 1,
            upper, lower,
            selenium_upper = 0, selenium_lower = 1, arsenic_upper = 0, arsenic_lower = 1,
            crvi_upper = 0, crvi_lower = 1, floride_upper = 0, floride_lower = 1;

    $.ajax({
        type: "GET",
        url: "assets/ContaminantData.xml",
        dataType: "xml",

        success: function (xml) {
            var $xml = $(xml);
            var $data = $xml.find('CONTAMINANTS');
            var $params = $xml.find('PARAMETERS');
            
            $data.each(function () {
                var contaminant = $(this).find('Contaminant_Type').text();

                lower = parseFloat($(this).find('Concentration_Lower').text());
                upper = parseFloat($(this).find('Concentration_Upper').text());

                if (contaminant === 'Selenium') {
                    if (upper > s_upper) {
                        selenium_upper = upper;
                        s_upper = upper;
                    }

                    if (lower < s_lower) {
                        selenium_lower = lower;
                        s_lower = lower;
                    }
                } else if (contaminant === 'Arsenic') {
                    if (upper > a_upper) {
                        arsenic_upper = upper;
                        a_upper = upper;
                    }

                    if (lower < a_lower) {
                        arsenic_lower = lower;
                        a_lower = lower;
                    }
                } else if (contaminant === 'Cr(VI)') {
                    if (upper > cr_upper) {
                        crvi_upper = upper;
                        cr_upper = upper;
                    }

                    if (lower < cr_lower) {
                        crvi_lower = lower;
                        cr_lower = lower;
                    }
                } else if (contaminant === 'Floride') {
                    if (upper > fl_upper) {
                        floride_upper = upper;
                        fl_upper = upper;
                    }

                    if (lower < fl_lower) {
                        floride_lower = lower;
                        fl_lower = lower;
                    }
                }
            });
            
            $params.each(function () {
                var mediaprice = $(this).find('Media_Price').text();
                var fillvolume = $(this).find('Media_Fill_Volume').text();
                var density = $(this).find('Density_of_Media').text();
				
				var hlrRangeMin = $(this).find('HLR_RANGE_MIN').text();
				var hlrRangeMax = $(this).find('HLR_RANGE_MAX').text();
				var heightMin = $(this).find('HEIGHT_MIN').text();
				var heightMax = $(this).find('HEIGHT_MAX').text();
				var pi = $(this).find('PI').text();
                
                xmlvalues.setParameters(density, fillvolume, mediaprice, hlrRangeMin, hlrRangeMax, heightMin, heightMax, pi);
            });
            
            xmlvalues.setSeleniumRange(selenium_upper, selenium_lower);
            xmlvalues.setArsenicRange(arsenic_upper, arsenic_lower);
            xmlvalues.setCrRange(crvi_upper, crvi_lower);
            xmlvalues.setFlorideRange(floride_upper, floride_lower);
            
            $("#sel_range").text("0 - " + selenium_upper);
            $("#a_range").text("0 - " + arsenic_upper);
            $("#cr_range").text("0 - " + crvi_upper);
            $("#f_range").text("0 - " + floride_upper);
        }
    });
});

$(document).ready(function () {
    $("#form").on('submit', function (e) {
        e.preventDefault();
		
        var type = $("input[name='treatmentType']:checked").val();
        var rtime = 0, temp = 0, hardness = 0, conductivity = 0, ph_upper = 0, ph_lower = 0, t_type, c_type;

        var selenium_entered = parseFloat($("#selenium_tb").val().trim()),
                arsenic_entered = parseFloat($("#arsenic_tb").val().trim()),
                cr_entered = parseFloat($("#cr_tb").val().trim()),
                floride_entered = parseFloat($("#floride_tb").val().trim());
        
        $.ajax({
            type: "GET",
            url: "assets/ContaminantData.xml",
            dataType: "xml",
            async: true,

            success: function (xml) {
                var $xml = $(xml);
                var $data = $xml.find('CONTAMINANTS');
				
                $data.each(function () {
                    var contaminant = $(this).find('Contaminant_Type').text();
                    var upper = parseFloat($(this).find('Concentration_Upper').text());
                    var lower = parseFloat($(this).find('Concentration_Lower').text());

                    if ((contaminant === 'Selenium' && (selenium_entered >= lower && selenium_entered <= upper))
                            || (contaminant === 'Arsenic' && (arsenic_entered >= lower && arsenic_entered <= upper))
                            || (contaminant === 'Cr(VI)' && (cr_entered >= lower && cr_entered <= upper))
                            || (contaminant === 'Floride' && (floride_entered >= lower && floride_entered <= upper))) {
                        if (type === 'Drinking Water') {
                            temp = parseFloat($(this).find('Reaction_Time_DW').text());
                        } else if (type === 'Waste Water') {
                            temp = parseFloat($(this).find('Reaction_Time_WW').text());
                        }
                    }

                    if (temp > rtime) {
                        rtime = temp;
                        hardness = $(this).find('Hardness').text();
                        conductivity = $(this).find('Conductivity').text();
                        ph_upper = $(this).find('pH_Need_Upper').text();
                        ph_lower = $(this).find('pH_Need_Lower').text();
                        t_type = $(this).find('Treatment_Type').text();
						c_type = contaminant;
                    }
                });
                
                xmlvalues.setPHRange(ph_upper, ph_lower);

                xmlvalues.setEBCT(rtime);
                xmlvalues.setTDS(hardness);
                xmlvalues.setConductivity(conductivity);
                xmlvalues.setTreatmentType(t_type);
                
                var validate = validateForm();

                if (validate) {
					var currTDS = $("#tds_tb").val().trim();
                    var currConductivity = $("#conductivity_tb").val().trim();
					var ph_val = $("#ph_tb").val().trim();
					
					var d = new Date();
					var dd = d.getDate();
					var month = d.getMonth(); //January is 0!
		
					if(dd < 10){
						dd= '0'+dd;
					} 
					var month_names = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
							
					var strDate = dd + "-" + month_names[month] + "-" +  d.getFullYear();
	
					$("#output_reportdate").text(strDate);
					$("#output_pn").text($("#projectname_tb").val().trim());
					$("#output_pl").text($("#projectleader_tb").val().trim());
					$("#output_cn").text($("#client_tb").val().trim());
					$("#output_wt").text($("#wastewater_tb").val().trim());
					if (floride_entered > 0){
						t_type = "EC";
						c_type = "Fluoride" ;
					}
        
					if(t_type === "LC") {
						$("#output_tt").text("Cleanit-LC :- " + $("#form input[name='treatmentType']:checked").val().trim());
					}
					else if(t_type === "EC") {
						$("#output_tt").text("Cleanit-EC :- " + $("#form input[name='treatmentType']:checked").val().trim());
					}
					var contaminantlist = [
						{ key: "selenium", val: selenium_entered },
						{ key: "arsenic", val: arsenic_entered },
						{ key: "cr", val: cr_entered },
						{ key: "floride", val: floride_entered }
					];
					contaminantlist.sort(function(a,b){
						if(a.val < b.val){ return 1}
						if(a.val > b.val){ return -1}
						return 0;
					});
					var count = 1;
					var labelname = "";
					var outputvalue = 0;
					var phOutOfRangeMessage = "Please consider adjusting the pH";
					var noOfContaminant = 0;
					var contaminantName = "";
					$.each(contaminantlist, function(key, obj) {
						
						 if (obj.val > 0)
						 {
							noOfContaminant = noOfContaminant + 1;
							if (obj.key === 'selenium'){
								labelname = "Selenium:";
								outputvalue = selenium_entered;
								contaminantName = "selenium";
							} 
							if (obj.key === 'arsenic'){
								labelname = "Arsenic:";
								outputvalue = arsenic_entered;
								contaminantName = "arsenic";
							} 
							if (obj.key === 'cr'){
								labelname = "Cr(VI):";
								outputvalue = cr_entered;
								contaminantName = "Cr(VI)";
							} 
							if (obj.key === 'floride'){
								labelname = "Fluoride:";
								outputvalue = floride_entered;
								contaminantName = "fluoride";
							} 
							 $("#output_label_" + count ).html(labelname);
							$("#output_" + count ).html(outputvalue);
						 }
						 else
						 {
							$("#output_div_" + count ).hide();
						 }
						count = count + 1;	 
					
					});
					
					if ( noOfContaminant == 1){
						phOutOfRangeMessage = phOutOfRangeMessage + " for " + contaminantName + " removal";
					}
					
					
					$("#output_q").html(parseFloat($("#q_tb").val().trim()));
					$("#output_ph").html(ph_val);
				
					if (type === 'Drinking Water') {
						$("#tds2").show();
						$("#conductivity2").hide();
						$("#output_tds2").html(currTDS);
						$("#output_tds2").digits();
					}
					else{
						$("#tds2").hide();
						$("#conductivity2").show();
						$("#output_ch2").html(currConductivity);
						$("#output_ch2").digits();
						
					}
					
                    if(t_type === "LC"){
                        var vm = 0, t = 0, mm = 0, vr = 0, tmp = 0;
						var hlrRangeMin = 0, hlrRangeMax = 0, heightMax = 0, heightMin = 0, pi = 0;
						var diaMin = 0, diaMax = 0;

                        var params = xmlvalues.getParameters();

                        var ebct = parseFloat(xmlvalues.getEBCT()),
                                q = parseFloat($("#q_tb").val()),
                                d = parseFloat(params.dm),
                                fv = parseFloat(params.fv),
                                mp = parseFloat(params.mp),
								hlrRangeMin = parseFloat(params.hlrRangeMin),
								hlrRangeMax = parseFloat(params.hlrRangeMax),
								heightMax = parseFloat(params.heightMax),
								heightMin = parseFloat(params.heightMin),
								pi = parseFloat(params.pi);
								
						

                        vm = (ebct / 60) * q;
                        vr = (vm / fv) * 100;
                        t = (vr / q) * 60;
                        mm = vm * (d / 1000000 * 1000000);
                        tmp = mp * mm;
						
						diaMin = Math.pow(((4 * q)/ ((pi) * hlrRangeMax)),0.5) ;
						diaMax = Math.pow((( 4 * q) / ((pi) * hlrRangeMin)),0.5);
						
						var tempMinHeightOfColumn = 0, tempMaxHeightOfColumn =0;
						tempMinHeightOfColumn = vr / (pi * Math.pow((diaMax/2),2));
						tempMaxHeightOfColumn = vr / (pi * Math.pow((diaMin/2),2));
						
						var minNoOfColumn = 1, maxNoOfColumn = 1;
						
						if (tempMinHeightOfColumn > heightMax)
						{
							minNoOfColumn = tempMinHeightOfColumn / heightMax ;
						}
						
						if (tempMaxHeightOfColumn > heightMax)
						{
							maxNoOfColumn = tempMaxHeightOfColumn / heightMax;
						}
						var minHeightFinal = 1, maxHeighFinal = 1;
						minHeightFinal = tempMinHeightOfColumn / minNoOfColumn;
						maxHeighFinal = tempMaxHeightOfColumn / maxNoOfColumn;
                         
                        
						$("#output_mv").html(vm.toFixed(2));
                        $("#output_thrt").html(t.toFixed(2));
                        $("#output_mq").html(mm.toFixed(2));
                        $("#output_trv").html(vr.toFixed(2));
						$("#output_ebct").html(ebct);
						
                        
						
						$("#output_hlr_min").html(hlrRangeMin);
						$("#output_hlr_max").html(hlrRangeMax);
										
						$("#output_hlr_from").text(hlrRangeMin);
						$("#output_hlr_to").text(hlrRangeMax);
						
						
						
						$("#output_height_from").html(Math.ceil(minHeightFinal));
						$("#output_height_from_ft").html(Math.ceil(minHeightFinal * 3.28084 ));
						$("#output_height_to").html(Math.ceil(maxHeighFinal));
						$("#output_height_to_ft").html(Math.ceil(maxHeighFinal * 3.28084));
						
						$("#output_columns_from").html(Math.ceil(minNoOfColumn));
						$("#output_columns_to").html(Math.ceil(maxNoOfColumn));
						
						
						$("#output_diameter_from").html(diaMin.toFixed(3));
						$("#output_diameter_from_ft").html((diaMin * 3.28084).toFixed(3));
						$("#output_diameter_to").html(diaMax.toFixed(3));
						$("#output_diameter_to_ft").html((diaMax * 3.28084).toFixed(3));
						
						$("#output_Price").html(tmp.toFixed(2));
						$("#output_Price").digits();
                    } 
					else {
                        $("#show_result input").val("");
                    }
                    
                    var $table = $("#comments_table tbody");
                    
                   
                    var tdsval = parseFloat(xmlvalues.getTDS());
                    var conductivityval = parseFloat(xmlvalues.getConductivity());
                    
                    $table.empty();
                    
                    if(t_type === "EC") {
                        var lcase = c_type.toLowerCase();
                        if(lcase === 'Fluoride' || lcase === 'fluoride') {
                            lcase = 'floride';
                        }
						if(lcase === 'Cr(VI)' || lcase === 'cr(vi)') {
                            lcase = 'cr';
                        }
                        $table.append("<tr><td>Please use EC system for this combination because " + c_type + " value is " + $("#" + lcase + "_tb").val().trim() + "</td></tr>");
                   
                    }
					if(t_type === "LC") {
						if (parseFloat(ph_val) < parseFloat(ph_lower) || parseFloat(ph_val) > parseFloat(ph_upper)) {
								$table.append("<tr><td>" + phOutOfRangeMessage +  "</td></tr>");
							}
                    
						if($("#treatmentType1")[0].checked && parseFloat(currTDS) > tdsval) {
								$table.append("<tr><td>Please adjust the TDS, current value " + currTDS + " is greater than " + tdsval + "</td></tr>");
							}
                    
						if($("#treatmentType2")[0].checked && parseFloat(currConductivity) < conductivityval) {
							$table.append("<tr><td>Please adjust the Conductivity, current value " + currConductivity + " is less than " + conductivityval + "</td></tr>");
						}
						
					}
                    if( rtime == 0 || isNaN(rtime)) {
							$table.append("<tr><td>No matching output. "  + "</td></tr>");
						}
                    
                    
                    if ($("#form input").hasClass("is-invalid")) {
                        $("#form input").removeClass("is-invalid");
                    }
                    
                    //var nRows = document.getElementById("comments_table").rows.length - 1;
                    //while(nRows < 4) {
                      //  $table.append("<tr><td>&nbsp;</td></tr>");
                       // nRows++;
                    //}

                    $("#form input").attr("readonly", "readonly");
                    $("#form input").addClass("disabled");
                    
                    var $hideType = $("#form input[name='treatmentType']:not(:checked)");
                    $hideType.attr("disabled", true);
                    $hideType.parent().hide();

                    $(".r-only").hide();
					$("#show_result").show();
                    $("#show_Input").hide();
					if( rtime == 0 || isNaN(rtime) || t_type === "EC" ) {
							$("#output_screen_value").hide();
						}
						else {
							$("#output_screen_value").show();
						}
                    $('html, body').animate({
                        scrollTop: $("#show_result").offset().top
                    }, 1000);
                }
            }
        });
    });

$.fn.digits = function(){ 
    return this.each(function(){ 
        $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ); 
    })
}   

   $("#resetBtn").on('click', function () {
        $("#form .invalid-tooltip").hide();
        if ($("#form input").hasClass("is-invalid")) {
            $("#form input").removeClass("is-invalid");
        }
    });

    $(document).on('change', '#treatmentType2:checked', function () {
        $("#form .invalid-tooltip").hide();
        $("#tds").hide();
        $("#conductivity").show();
    });

    $(document).on('change', '#treatmentType1:checked', function () {
        $("#form .invalid-tooltip").hide();
        $("#conductivity").hide();
        $("#tds").show();
    });
    
    $(document).on('change', '#ph_tb', function () {
        $("#form .invalid-tooltip").hide();
    });
    
    $("#restartBtn").on('click', function () {
        $("#show_result").hide();
		$("#show_Input").show();
        $(".r-only").show();

        $("#form input").removeAttr("readonly");
        $("#form input").removeClass("disabled");
        
        var $hideType = $("#form input[name='treatmentType']:not(:checked)");
        $hideType.removeAttr("disabled");
        $hideType.parent().show();

        $("#form")[0].reset();
        $("#show_result input").val("");
    });
    
	function printform(removePrice){
		if(removePrice){
			$("#output_Price").hide();
			$("#label_Price").hide();
			$("#output_PriceUnit_Label").hide();
			
		}
		$("#header_section").hide();
		$("#button_section").hide();
		window.print();
		$("#header_section").show();
		$("#button_section").show();
		if(removePrice){
			$("#output_Price").show();
			$("#label_Price").show();
			$("#output_PriceUnit_Label").show();
		}
        return false;
	}
	
    $("#printPrevBtn").on('click', function() {
		 return printform (false);
        
        
       
    });
	$("#noPricePrintPrevBtn").on('click', function() {
		 return printform (true);
        
        
       
    });
});